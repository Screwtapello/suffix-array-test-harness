#!/usr/bin/python3
"""
An implementation of the SA-IS ("Suffix Array - Induced Sorting") algorithm.

As described in the paper "Linear Suffix Array Construction by Almost Pure
Induced-Sorting", by Ge Nong, Sen Zhang and Wai Hong Chan, although there's
a slightly more detailed description in "Two Efficient Algorithms for Linear
Time Suffix Array Construction" by the same authors. Where the paper glossed
over the algorithm details, I've referred to the example C++ implementation
available from https://code.google.com/p/ge-nong/

According to the paper, the basic steps of the algorithm are:

    - Scan the input string to classify all the characters as L- or S-type,
      producing a type map of the string.
    - Scan the type map to find all the LMS substrings of the string, producing
      an array of substrings.
    - Induced-sort the array of substrings.
    - Construct a summary of the input string, where each of the substrings is
      replaced by a single character.
    - If any characters appear more than once in the summary:
        - Recurse, to calculate the sorted suffix-array of the summary string.
    - Otherwise, just directly calculate the sorted suffix-array of the summary
      string.
    - Use the induced-sort algorithm to construct the sorted suffix-array of
      the input string from the sorted suffix-array of the summary string.

For more discussion of the steps of the above algorithm, see the papers
mentioned.
"""


# Constants for recording whether a character in a string is S-type or L-type.
S_TYPE = ord("S")
L_TYPE = ord("L")


def _showArray(arr, pos=None):
    for item in arr:
        print(" %02d" % item, end="")

    print()

    if pos is not None:
        print("   " * pos, "^^")


def buildTypeMap(data):
    """
    Builds a map marking each suffix of the data as S_TYPE or L_TYPE.

    S_TYPE means the suffix starting from this character is smaller
    than the suffix starting from the next character; L_TYPE means it
    is larger.
    """
    # The map should contain one more entry than there are characters
    # in the string, because we also need to store the type of the
    # empty suffix between the last character and the end of the
    # string.
    res = bytearray(len(data) + 1)

    # The null suffix after the last character is defined to be S_TYPE
    res[-1] = S_TYPE

    # If this is an empty string...
    if not len(data):
        # ...there are no more characters, so we're done.
        return res

    # The suffix containing only the last character must necessarily be
    # larger than the empty suffix.
    res[-2] = L_TYPE

    # Step through the rest of the string from right to left.
    for i in range(len(data)-2, -1, -1):
        if data[i] > data[i+1]:
            res[i] = L_TYPE
        elif data[i] == data[i+1] and res[i+1] == L_TYPE:
            res[i] = L_TYPE
        else:
            res[i] = S_TYPE

    return res


def isLMSChar(offset, typemap):
    """
    Returns true if the character at offset is a leftmost S-type.
    """
    if offset == 0:
        return False
    if typemap[offset] == S_TYPE and typemap[offset - 1] == L_TYPE:
        return True

    return False


def showTypeMap(data):
    typemap = buildTypeMap(data)

    print(data.decode('ascii'))
    print(typemap.decode('ascii'))

    print("".join(
            "^" if isLMSChar(i, typemap) else " "
            for i in range(len(typemap))
        ))


def lmsSubstringsAreEqual(string, typemap, offsetA, offsetB):
    """
    Return True if the substrings at offsetA and offsetB are equal.

    Although section 2.2 of the paper says that LMS substrings are
    equal if they have the same content, the same types, and the same
    length, due to the definition of "LMS substring" I can't figure out
    how to construct two substrings with the same length but different
    types.
    """
    # The typemap should have one extra entry at the end for the empty
    # suffix.
    assert len(typemap) == len(string) + 1

    # Initial offsets must be within the range of the typemap.
    assert 0 <= offsetA < len(typemap)
    assert 0 <= offsetB < len(typemap)

    # Initial offsets must point at LMS characters
    assert isLMSChar(offsetA, typemap)
    assert isLMSChar(offsetB, typemap)

    # Every LMS substring is equal to itself.
    if offsetA == offsetB:
        return True

    # No other substring is equal to the empty suffix.
    if offsetA == len(string) or offsetB == len(string):
        return False

    i = 0
    while True:
        aIsLMS = isLMSChar(i + offsetA, typemap)
        bIsLMS = isLMSChar(i + offsetB, typemap)

        # If we've found the start of the next LMS substrings...
        if (i > 0 and aIsLMS and bIsLMS):
            # ...then we made it all the way through our original LMS
            # substrings without finding a difference, so we can go
            # home now.
            return True

        if aIsLMS != bIsLMS:
            # We found the end of one LMS substring before we reached
            # the end of the other.
            return False

        if string[i + offsetA] != string[i + offsetB]:
            # We found a character difference, we're done.
            return False

        i += 1


def findBucketSizes(string, alphabetSize=256):
    """
    Returns an array of bucket-sizes big enough to store string's characters.

    'string' is an arbitrary, finite sequence of integers, for example
        individual bytes.
    'alphabetSize' is the number of distinct values each item of 'string' might
        have. If 'string' is a byte-string, 'alphabetSize' will be 256, but if
        'string' is a sequence of other integers it may have a different value.

    The return value is an array of 'alphabetSize' elements, where each element
    is the count of the times that character occurred in the string.
    """
    res = [0] * alphabetSize

    for char in string:
        res[char] += 1

    return res


def findBucketHeads(bucketSizes):
    """
    Returns an array of offsets to the start of each bucket.
    """
    offset = 1 # The null suffix lives at offset 0.
    res = []
    for size in bucketSizes:
        res.append(offset)
        offset += size

    return res


def findBucketTails(bucketSizes):
    """
    Returns an array of offsets to the tail of each bucket.
    """
    offset = 1 # The null suffix lives at offset 0.
    res = []
    for size in bucketSizes:
        offset += size
        res.append(offset - 1)

    return res


def induceSortL(string, suffixOffsets, bucketSizes, typemap):
    """
    Phase 2 of induced sorting: slotting L-type suffixes into place.

    Now we have the LMS-substrings in approximately the right place, we can
    scan through the array again, finding L-type suffixes next to
    already-placed suffixes, and placing them in the correct buckets.
    """
    print("Inducing sort: L-type suffixes")
    bucketHeads = findBucketHeads(bucketSizes)

    for i in range(len(suffixOffsets)):
        if suffixOffsets[i] == -1:
            # No offset is recorded here.
            continue

        # We're interested in the suffix that begins to the left of the suffix
        # this entry points at.
        j = suffixOffsets[i] - 1
        if typemap[j] != L_TYPE:
            # We're only interested in L-type suffixes right now.
            continue

        # Which bucket does this suffix go into?
        bucketIndex = string[j]
        # Add the start position at the head of the bucket...
        suffixOffsets[bucketHeads[bucketIndex]] = j
        # ...and move the head pointer up.
        bucketHeads[bucketIndex] += 1

        _showArray(suffixOffsets, i)


def induceSortS(string, suffixOffsets, bucketSizes, typemap):
    """
    Phase 3 of induced sorting: slotting S-type suffixes into place.

    Now we have all the L-type suffixes in approximately the right
    place, we can scan through the array one more time, finding S-type
    suffixes and putting them in the correct buckets. This will clobber the
    LMS-suffixes we placed in step 1. but they'll be reconstructed in the
    correct places.
    """
    print("Inducing sort: S-type suffixes")
    bucketTails = findBucketTails(bucketSizes)

    for i in range(len(suffixOffsets)-1, -1, -1):
        j = suffixOffsets[i] - 1
        if j < 0:
            # There is no useful information in the typemap at this location;
            # skip it.
            continue
        if typemap[j] != S_TYPE:
            # We're only interested in S-type suffixes right now.
            continue

        # Which bucket does this suffix go into?
        bucketIndex = string[j]
        # Add the start position at the tail of the bucket...
        suffixOffsets[bucketTails[bucketIndex]] = j
        # ...and move the tail pointer down.
        bucketTails[bucketIndex] -= 1

        _showArray(suffixOffsets, i)


def guessLMSSort(string, bucketSizes, typemap):
    """
    Step 1, phase 1: Make a suffix array with LMS-substrings approximately right.
    """
    # Create a suffix array with room for a pointer to every suffix of the
    # string, including the null suffix at the end.
    suffixOffsets = [-1] * (len(string) + 1)

    bucketTails = findBucketTails(bucketSizes)

    # The example in the paper does this loop in ascending order; the reference
    # code does it in descending order.
    # Ascending is simpler, and works identically, so let's do that.
    for i in range(len(string)):
        if not isLMSChar(i, typemap):
            continue

        # Which bucket does this suffix go into?
        bucketIndex = string[i]
        # Add the start position at the tail of the bucket...
        suffixOffsets[bucketTails[bucketIndex]] = i
        # ...and move the tail pointer down.
        bucketTails[bucketIndex] -= 1

        _showArray(suffixOffsets)

    # The null suffix is defined to be an LMS-substring, and we know it goes at
    # the front.
    suffixOffsets[0] = len(string)

    _showArray(suffixOffsets)

    return suffixOffsets


def summariseSuffixArray(string, suffixOffsets, typemap):
    """
    Step 2: construct a 'summary string' of the positions of LMS-substrings.

    The summary string is a sequence of characters, where each distinct
    character represents a distinct LMS-substring in the original string.
    LMS-substrings are allocated characters in the order that they appear in
    suffixOffsets, but the characters appear in the summary string in the same
    order that the LMS-substrings appeared in the original string.
    """
    lmsNames = [-1] * (len(string) + 1)

    # Keep count of what names we've allocated.
    currName = 0

    # What was the last LMS-substring we checked?
    lastLMSSubstring = None

    # We always know that the first LMS-substring we'll see is the one
    # representing the null suffix, and it will always be at position 0 of the
    # suffixOffsets.
    lmsNames[suffixOffsets[0]] = currName
    lastLMSSubstring = suffixOffsets[0]

    for i in range(1, len(suffixOffsets)):
        stringOffset = suffixOffsets[i]

        if not isLMSChar(stringOffset, typemap):
            continue

        if not lmsSubstringsAreEqual(string, typemap,
                lastLMSSubstring, stringOffset):
            # This LMS-substring is different from the previous one we found.
            currName += 1
            lastLMSSubstring = stringOffset

        lmsNames[stringOffset] = currName
        _showArray(lmsNames, stringOffset)

    # Now we have all the parts of summaryString named in the right order, and
    # stored in the right order, but we've got a bunch of untouched empty items
    # in that sequence we don't care about. We also take this opportunity to
    # build lmsOffsets, which tells us which LMS-substring each item in the
    # summary string represents. This will be important later.
    lmsOffsets = []
    summaryString = []
    for index, name in enumerate(lmsNames):
        if name == -1:
            continue
        lmsOffsets.append(index)
        summaryString.append(name)

    _showArray(summaryString)

    # The lexicographically smallest character in the summary string is
    # numbered zero, so the total number of characters in our alphabet is one
    # larger than the largest numbered character.
    summaryAlphabetSize = currName + 1

    return summaryString, summaryAlphabetSize, lmsOffsets


def makeSummarySuffixArray(summaryString, summaryAlphabetSize):
    """
    Step 3: Construct a sorted suffix array of the summary string.
    """
    # If there are fewer names than there are items in lmsOffsets, then we have
    # some duplicate LMS-substrings, and we'll need to solve this recursively.
    if summaryAlphabetSize < len(summaryString):
        summarySuffixArray = _sais(summaryString, summaryAlphabetSize)

    else:
        # We hit the end of the summary-string, and the number of distinct
        # names is equal to the number of items in the summary-string. This
        # means that every name occurs exactly once. Because the names are
        # consecutive integers starting from 0, after sorting the suffixes the
        # first suffix will be the one beginning with 0, then the suffix
        # beginning with 1, then with 2, and so forth.
        #
        # So, we can very easily construct the suffix array of our summary
        # string: if there is an item at offset X in the summary string with
        # a value of Y, then that means the item at offset Y of the suffix
        # array should be set to a value of X.
        summarySuffixArray = [-1] * (len(summaryString) + 1)
        for x in range(len(summaryString)):
            y = summaryString[x]
            summarySuffixArray[y + 1] = x

    _showArray(summarySuffixArray)

    return summarySuffixArray


def accurateLMSSort(string, bucketSizes, typemap, summarySuffixArray, lmsOffsets):
    """
    Step 4, phase 1: Make a suffix array with LMS-substrings exactly right.
    """
    # summarySuffixArray tells us how to sort the items in summaryString... but
    # each item in summaryString represents an LMS-substring in our original
    # string, so we need to figure out which LMS-substring each item in the
    # summaryString refers to.

    # We need to seed these LMS offsets into our new suffix array, in the order
    # dictated by summarySuffixArray. As before, we'll be adding them to
    # the ends of their respective buckets, so to keep them in the right order
    # we'll have to iterate through summarySuffixArray in reverse order.
    suffixOffsets = [-1] * (len(string) + 1)

    # We know our suffix array will have the null suffix at the front.
    suffixOffsets[0] = len(string)

    bucketTails = findBucketTails(bucketSizes)
    for i in range(len(summarySuffixArray)-1, 1, -1):
        print(i)
        stringIndex = lmsOffsets[summarySuffixArray[i]]

        # Which bucket does this suffix go into?
        bucketIndex = string[stringIndex]
        # Add the suffix at the tail of the bucket...
        suffixOffsets[bucketTails[bucketIndex]] = stringIndex
        # ...and move the tail pointer down.
        bucketTails[bucketIndex] -= 1

        _showArray(suffixOffsets)

    _showArray(suffixOffsets)

    return suffixOffsets


def _sais(string, alphabetSize):
    """
    Recursively compute the suffix array of 'string'.
    """

    # Classify each character of the string as S_TYPE or L_TYPE
    typemap = buildTypeMap(string)

    # We'll be slotting suffixes into buckets according to what character they
    # start with, so let's precompute that info now.
    bucketSizes = findBucketSizes(string, alphabetSize)

    # Use induced sorting to create a 'first draft' sort of the suffixes of
    # 'string'.
    #
    # As per section 2.2 of the paper, like any lexicographically
    # sorted list, all the suffixes beginning with the same character will
    # necessarily be clumped together in groups. Furthermore, within each of
    # those groups, all the suffixes beginning with an S_TYPE character will
    # occur after all the suffixes beginning with an L_TYPE character.

    print("Step 1:")
    # Step 1: insert the LMS-substrings into suffixOffsets in our best-guess
    # location based on the bucket they fall into.
    suffixOffsets = guessLMSSort(string, bucketSizes, typemap)

    # Now we have the LMS-substrings in *approximately* the correct position,
    # use induced sorting to fill out all the other entries in the suffix
    # array. This may reshuffle the LMS-substrings into something closer to
    # their proper order.
    induceSortL(string, suffixOffsets, bucketSizes, typemap)
    induceSortS(string, suffixOffsets, bucketSizes, typemap)

    print("Step 2:")
    # "Naming" the LMS-substrings (the substrings beginning at the same place
    # of each of our LMS-suffixes, and running to the beginning of the next
    # LMS-suffix). This produces a new string which summarises the pattern of
    # LMS-suffixes in the original string.
    summaryString, summaryAlphabetSize, lmsOffsets = summariseSuffixArray(
            string, suffixOffsets, typemap)

    print("Step 3:")
    summarySuffixArray = makeSummarySuffixArray(
            summaryString, summaryAlphabetSize)

    print("Step 4:")
    suffixOffsets = accurateLMSSort(string, bucketSizes, typemap,
            summarySuffixArray, lmsOffsets)

    induceSortL(string, suffixOffsets, bucketSizes, typemap)
    induceSortS(string, suffixOffsets, bucketSizes, typemap)

    _showArray(suffixOffsets)

    return suffixOffsets


def suffixsort(bytestring):
    return _sais(bytestring, 256)
