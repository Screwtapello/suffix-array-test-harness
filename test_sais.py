#!/usr/bin/python3
"""
Test our various SA-IS implementations
"""
import pathlib
import unittest
import sais_ref
import sais


class SuffixSortTestMixin:
	"""
	Tests for a suffix-sort implementation.
	"""

	def _suffix_sort(self, bytestring):
		"""
		Invoke a suffix-sort implementation.

		Implementations should take a bytestring and return a list of integers
		representing the resulting suffix array.
		"""
		raise NotImplementedError()

	def _naive_sort(self, bytestring):
		"""
		A naive, slow suffix-sort implementation.
		"""
		suffixes = [bytestring[x:] for x in range(len(bytestring)+1)]
		suffixes.sort()
		return [len(bytestring) - len(x) for x in suffixes]

	def _compare_implementations(self, bytestring):
		"""
		Compare this implementation with the naive implementation.
		"""
		print(repr(bytestring))
		self.assertEqual(
				self._suffix_sort(bytestring),
				self._naive_sort(bytestring),
			)

	def test_empty(self):
		"""
		The empty string has an empty suffix array.
		"""
		self._compare_implementations(b'')

	def test_simple(self):
		"""
		A sort that can be done in one pass of induced-sorting.
		"""
		self._compare_implementations(b"ococonut")

	def test_medium(self):
		"""
		A sort that can be done in two passes of induced-sorting.
		"""
		self._compare_implementations(b"bababad")

	def test_longer(self):
		"""
		A sort that requires a recursion.
		"""
		self._compare_implementations(b"mmiissiissiippii")

	def test_random(self):
		"""
		Let's sort some randomly-generated string.
		"""
		with open("/dev/urandom", "rb") as handle:
			self._compare_implementations(handle.read(12))

	def test_with_NUL(self):
		"""
		An embedded NUL does not confuse the sort algorithm.
		"""
		self._compare_implementations(b"bana\0na")

	def test_byuu_beat_source(self):
		"""
		The "source" blob that confused byuu's SAIS implementation.
		"""
		with pathlib.Path(__file__).with_name("source-bin.txt").open() as fd:
			self._compare_implementations(
				bytes(int(line) for	line in	fd)
	        )

	def test_byuu_beat_target(self):
		"""
		The "target" blob that confused byuu's SAIS implementation.
		"""
		with pathlib.Path(__file__).with_name("target-bin.txt").open() as fd:
			self._compare_implementations(
				bytes(int(line) for	line in	fd)
	        )


class SAISRefTestCase(unittest.TestCase, SuffixSortTestMixin):
	"""
	Apply the standard tests to the reference implementation.
	"""

	def _suffix_sort(self, bytestring):
		return sais_ref.suffixsort(bytestring).tolist()


class MySAISTestCase(unittest.TestCase, SuffixSortTestMixin):
	"""
	Apply the standard tests to my reimplementation.
	"""

	def _suffix_sort(self, bytestring):
		return sais.suffixsort(bytestring)

	def test_compare_lms_match(self):
		self.assertEqual(
				sais.lmsSubstringsAreEqual(
					b"dabcdabcdab", b"LSSSLSSSLSLS", 1, 5,
				),
				True,
			)

	def test_compare_lms_char_mismatch(self):
		self.assertEqual(
				sais.lmsSubstringsAreEqual(
					b"dabcdabce", b"LSSSLSSSLS", 1, 5,
				),
				False,
			)

	def test_compare_lms_type_mismatch(self):
		self.assertEqual(
				sais.lmsSubstringsAreEqual(
					b"dabcdabcde", b"LSSSLSSSSLS", 1, 5,
				),
				False,
			)


if __name__ == "__main__":
	unittest.main()
