#!/usr/bin/python3
"""
A Python wrapper around the C++ reference implementation.
"""
import os
import subprocess
import tempfile
import unittest
from array import array

BASEDIR=os.path.dirname(__file__)

# Before we go any further, let's make sure we have the reference
# implementation around to call.
subprocess.check_call(["make", "-C", os.path.join(BASEDIR, "sa-is"), "is"])

def suffixsort(bytestring):
	# The reference implementation deliberately doesn't support an empty input,
	# for whatever reason.
	if bytestring == b"":
		return array('I', [0])

	# Despite claims to the contrary, the reference implementation doesn't seem
	# to handle reading from stdin.
	inputfile = tempfile.NamedTemporaryFile()
	inputfile.write(bytestring)
	inputfile.flush()

	# Hand the temporary file to the subprocess, and read the suffix array from
	# stdout.
	sp = subprocess.Popen(
			[os.path.join(BASEDIR, "sa-is", "is"), inputfile.name],
			stdin=subprocess.PIPE, stdout=subprocess.PIPE)
	suffixarraydata, _ = sp.communicate(bytestring)

	# We're done with this temporary input file.
	inputfile.close()

	# Deserialise the suffix array.
	suffixarray = array('I')
	suffixarray.frombytes(suffixarraydata)

	return suffixarray
